package com.yanxi.yxregiserve.web.controller.system;

import com.yanxi.yxregiserve.web.controller.model.BaseController;
import com.yanxi.yxregiserve.web.domin.model.AjaxResult;
import com.yanxi.yxregiserve.web.domin.system.ClientExample;
import com.yanxi.yxregiserve.web.domin.system.ConfigData;
import com.yanxi.yxregiserve.web.service.system.ClientExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/clientExamples")
public class ClientExampleController extends BaseController {

    @Autowired
    private ClientExampleService clientExampleService;


    /**
     * 查询实例列表
     */
    @GetMapping("/list")
    public AjaxResult list(ClientExample clientExample)
    {
        List<ClientExample> list = clientExampleService.selectTList(clientExample);
        return AjaxResult.success(list);
    }


    /**
     * 根据id查询实例信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(clientExampleService.selectTById(id));
    }

    /**
     * 新增实例
     */
    @PostMapping
    public AjaxResult add(@RequestBody ClientExample clientExample)
    {
        return toAjax(clientExampleService.insertT(clientExample)) ;
    }

    /**
     * 修改实例
     */
    @PutMapping
    public AjaxResult edit(@RequestBody ClientExample clientExample)
    {
        return toAjax(clientExampleService.updateT(clientExample));
    }

    /**
     * 删除实例
     */
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(clientExampleService.deleteTById(id));
    }
}
