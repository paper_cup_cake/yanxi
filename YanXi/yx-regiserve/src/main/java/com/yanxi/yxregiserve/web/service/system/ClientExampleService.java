package com.yanxi.yxregiserve.web.service.system;

import com.yanxi.yxregiserve.web.domin.system.ClientExample;
import com.yanxi.yxregiserve.web.service.model.BaseService;

public interface ClientExampleService extends BaseService<ClientExample> {
}
