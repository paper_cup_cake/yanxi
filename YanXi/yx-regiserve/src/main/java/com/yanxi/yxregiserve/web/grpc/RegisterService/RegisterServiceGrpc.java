package com.yanxi.yxregiserve.web.grpc.RegisterService;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.34.1)",
    comments = "Source: register.proto")
public final class RegisterServiceGrpc {

  private RegisterServiceGrpc() {}

  public static final String SERVICE_NAME = "RegisterService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterRequest,
      com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterResponse> getRegisterClientMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "registerClient",
      requestType = com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterRequest.class,
      responseType = com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterRequest,
      com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterResponse> getRegisterClientMethod() {
    io.grpc.MethodDescriptor<com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterRequest, com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterResponse> getRegisterClientMethod;
    if ((getRegisterClientMethod = RegisterServiceGrpc.getRegisterClientMethod) == null) {
      synchronized (RegisterServiceGrpc.class) {
        if ((getRegisterClientMethod = RegisterServiceGrpc.getRegisterClientMethod) == null) {
          RegisterServiceGrpc.getRegisterClientMethod = getRegisterClientMethod =
              io.grpc.MethodDescriptor.<com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterRequest, com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "registerClient"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterResponse.getDefaultInstance()))
              .setSchemaDescriptor(new RegisterServiceMethodDescriptorSupplier("registerClient"))
              .build();
        }
      }
    }
    return getRegisterClientMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.yanxi.yxregiserve.web.grpc.RegisterService.HealthRequest,
      com.yanxi.yxregiserve.web.grpc.RegisterService.HealthResponse> getHealthcheckMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "healthcheck",
      requestType = com.yanxi.yxregiserve.web.grpc.RegisterService.HealthRequest.class,
      responseType = com.yanxi.yxregiserve.web.grpc.RegisterService.HealthResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.yanxi.yxregiserve.web.grpc.RegisterService.HealthRequest,
      com.yanxi.yxregiserve.web.grpc.RegisterService.HealthResponse> getHealthcheckMethod() {
    io.grpc.MethodDescriptor<com.yanxi.yxregiserve.web.grpc.RegisterService.HealthRequest, com.yanxi.yxregiserve.web.grpc.RegisterService.HealthResponse> getHealthcheckMethod;
    if ((getHealthcheckMethod = RegisterServiceGrpc.getHealthcheckMethod) == null) {
      synchronized (RegisterServiceGrpc.class) {
        if ((getHealthcheckMethod = RegisterServiceGrpc.getHealthcheckMethod) == null) {
          RegisterServiceGrpc.getHealthcheckMethod = getHealthcheckMethod =
              io.grpc.MethodDescriptor.<com.yanxi.yxregiserve.web.grpc.RegisterService.HealthRequest, com.yanxi.yxregiserve.web.grpc.RegisterService.HealthResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "healthcheck"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yanxi.yxregiserve.web.grpc.RegisterService.HealthRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yanxi.yxregiserve.web.grpc.RegisterService.HealthResponse.getDefaultInstance()))
              .setSchemaDescriptor(new RegisterServiceMethodDescriptorSupplier("healthcheck"))
              .build();
        }
      }
    }
    return getHealthcheckMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static RegisterServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<RegisterServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<RegisterServiceStub>() {
        @java.lang.Override
        public RegisterServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new RegisterServiceStub(channel, callOptions);
        }
      };
    return RegisterServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static RegisterServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<RegisterServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<RegisterServiceBlockingStub>() {
        @java.lang.Override
        public RegisterServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new RegisterServiceBlockingStub(channel, callOptions);
        }
      };
    return RegisterServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static RegisterServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<RegisterServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<RegisterServiceFutureStub>() {
        @java.lang.Override
        public RegisterServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new RegisterServiceFutureStub(channel, callOptions);
        }
      };
    return RegisterServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class RegisterServiceImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * 注册
     * </pre>
     */
    public void registerClient(com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterRequest request,
        io.grpc.stub.StreamObserver<com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getRegisterClientMethod(), responseObserver);
    }

    /**
     * <pre>
     * 健康检测
     * </pre>
     */
    public void healthcheck(com.yanxi.yxregiserve.web.grpc.RegisterService.HealthRequest request,
        io.grpc.stub.StreamObserver<com.yanxi.yxregiserve.web.grpc.RegisterService.HealthResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getHealthcheckMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getRegisterClientMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterRequest,
                com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterResponse>(
                  this, METHODID_REGISTER_CLIENT)))
          .addMethod(
            getHealthcheckMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.yanxi.yxregiserve.web.grpc.RegisterService.HealthRequest,
                com.yanxi.yxregiserve.web.grpc.RegisterService.HealthResponse>(
                  this, METHODID_HEALTHCHECK)))
          .build();
    }
  }

  /**
   */
  public static final class RegisterServiceStub extends io.grpc.stub.AbstractAsyncStub<RegisterServiceStub> {
    private RegisterServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RegisterServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new RegisterServiceStub(channel, callOptions);
    }

    /**
     * <pre>
     * 注册
     * </pre>
     */
    public void registerClient(com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterRequest request,
        io.grpc.stub.StreamObserver<com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getRegisterClientMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     * <pre>
     * 健康检测
     * </pre>
     */
    public void healthcheck(com.yanxi.yxregiserve.web.grpc.RegisterService.HealthRequest request,
        io.grpc.stub.StreamObserver<com.yanxi.yxregiserve.web.grpc.RegisterService.HealthResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getHealthcheckMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class RegisterServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<RegisterServiceBlockingStub> {
    private RegisterServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RegisterServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new RegisterServiceBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * 注册
     * </pre>
     */
    public com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterResponse registerClient(com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterRequest request) {
      return blockingUnaryCall(
          getChannel(), getRegisterClientMethod(), getCallOptions(), request);
    }

    /**
     * <pre>
     * 健康检测
     * </pre>
     */
    public com.yanxi.yxregiserve.web.grpc.RegisterService.HealthResponse healthcheck(com.yanxi.yxregiserve.web.grpc.RegisterService.HealthRequest request) {
      return blockingUnaryCall(
          getChannel(), getHealthcheckMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class RegisterServiceFutureStub extends io.grpc.stub.AbstractFutureStub<RegisterServiceFutureStub> {
    private RegisterServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RegisterServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new RegisterServiceFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * 注册
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterResponse> registerClient(
        com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getRegisterClientMethod(), getCallOptions()), request);
    }

    /**
     * <pre>
     * 健康检测
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.yanxi.yxregiserve.web.grpc.RegisterService.HealthResponse> healthcheck(
        com.yanxi.yxregiserve.web.grpc.RegisterService.HealthRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getHealthcheckMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_REGISTER_CLIENT = 0;
  private static final int METHODID_HEALTHCHECK = 1;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final RegisterServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(RegisterServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_REGISTER_CLIENT:
          serviceImpl.registerClient((com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterRequest) request,
              (io.grpc.stub.StreamObserver<com.yanxi.yxregiserve.web.grpc.RegisterService.RegisterResponse>) responseObserver);
          break;
        case METHODID_HEALTHCHECK:
          serviceImpl.healthcheck((com.yanxi.yxregiserve.web.grpc.RegisterService.HealthRequest) request,
              (io.grpc.stub.StreamObserver<com.yanxi.yxregiserve.web.grpc.RegisterService.HealthResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class RegisterServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    RegisterServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.yanxi.yxregiserve.web.grpc.RegisterService.Register.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("RegisterService");
    }
  }

  private static final class RegisterServiceFileDescriptorSupplier
      extends RegisterServiceBaseDescriptorSupplier {
    RegisterServiceFileDescriptorSupplier() {}
  }

  private static final class RegisterServiceMethodDescriptorSupplier
      extends RegisterServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    RegisterServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (RegisterServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new RegisterServiceFileDescriptorSupplier())
              .addMethod(getRegisterClientMethod())
              .addMethod(getHealthcheckMethod())
              .build();
        }
      }
    }
    return result;
  }
}
