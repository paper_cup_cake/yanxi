package com.yanxi.yxregiserve.web.service.system;

import com.yanxi.yxregiserve.web.domin.system.ConfigData;
import com.yanxi.yxregiserve.web.service.model.BaseService;

public interface ConfigDataService extends BaseService<ConfigData> {
}
