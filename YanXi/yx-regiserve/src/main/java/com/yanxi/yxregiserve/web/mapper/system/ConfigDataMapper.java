package com.yanxi.yxregiserve.web.mapper.system;

import com.yanxi.yxregiserve.web.domin.system.ConfigData;
import com.yanxi.yxregiserve.web.mapper.model.BaseMapper;
import java.util.Map;

//@Mapper
public interface ConfigDataMapper extends BaseMapper<ConfigData> {

}
