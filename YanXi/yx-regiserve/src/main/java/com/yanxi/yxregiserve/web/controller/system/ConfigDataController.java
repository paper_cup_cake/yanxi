package com.yanxi.yxregiserve.web.controller.system;

import com.yanxi.yxregiserve.web.controller.model.BaseController;
import com.yanxi.yxregiserve.web.domin.model.AjaxResult;
import com.yanxi.yxregiserve.web.domin.system.ConfigData;
import com.yanxi.yxregiserve.web.service.system.ConfigDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/configDatas")
public class ConfigDataController extends BaseController {

    @Autowired
    private ConfigDataService configDataService;

    /**
     * 查询配置列表
     */
    @GetMapping("/list")
    public AjaxResult list(ConfigData configData)
    {
        List<ConfigData> list = configDataService.selectTList(configData);
        return AjaxResult.success(list);
    }


    /**
     * 根据id查询配置信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(configDataService.selectTById(id));
    }

    /**
     * 新增配置
     */
    @PostMapping
    public AjaxResult add(@RequestBody ConfigData configData)
    {
        return toAjax(configDataService.insertT(configData)) ;
    }

    /**
     * 修改配置
     */
    @PutMapping
    public AjaxResult edit(@RequestBody ConfigData configData)
    {
        return toAjax(configDataService.updateT(configData));
    }

    /**
     * 删除配置
     */
    @DeleteMapping("/{id}")
    public AjaxResult remove(@PathVariable Long id)
    {
        return toAjax(configDataService.deleteTById(id));
    }

}
