package com.yanxi.yxregiserve;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
public class YxRegiserveApplication {

    public static void main(String[] args) {
        SpringApplication.run(YxRegiserveApplication.class, args);
        System.out.println("(⑅•ᴗ•⑅)◜..°♡  YanXi项目 启动成功  ❛‿˂̵✧");
    }

}
