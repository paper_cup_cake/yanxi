package com.yanxi.yxregiserve.web.service.model;

import java.util.List;

public interface BaseService<T> {

    /**
     * id查询
     *
     * @param id 自增id
     * @return
     */
    public T selectTById(Long id);

    /**
     * 条件查询
     *
     * @param t 对象
     * @return 对象集合
     */
    public List<T> selectTList(T t);

    /**
     * 新增
     *
     * @param t 对象
     * @return 结果
     */
    public int insertT(T t);

    /**
     * 修改
     *
     * @param t 对象
     * @return 结果
     */
    public int updateT(T t);

    /**
     * 批量删除
     *
     * @param ids
     * @return 结果
     */
    public int deleteTByIds(Long[] ids);

    /**
     * id删除
     *
     * @param id 自增id
     * @return 结果
     */
    public int deleteTById(Long id);

}

