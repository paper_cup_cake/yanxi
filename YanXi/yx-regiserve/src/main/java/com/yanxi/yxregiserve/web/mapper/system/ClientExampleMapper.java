package com.yanxi.yxregiserve.web.mapper.system;

import com.yanxi.yxregiserve.web.domin.system.ClientExample;
import com.yanxi.yxregiserve.web.mapper.model.BaseMapper;

public interface ClientExampleMapper extends BaseMapper<ClientExample> {
}
