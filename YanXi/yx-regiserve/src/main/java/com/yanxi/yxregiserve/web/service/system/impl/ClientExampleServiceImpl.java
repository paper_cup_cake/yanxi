package com.yanxi.yxregiserve.web.service.system.impl;

import com.yanxi.yxregiserve.web.domin.system.ClientExample;
import com.yanxi.yxregiserve.web.mapper.system.ClientExampleMapper;
import com.yanxi.yxregiserve.web.service.system.ClientExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientExampleServiceImpl implements ClientExampleService {

    @Autowired
    private ClientExampleMapper clientExampleMapper;

    @Override
    public ClientExample selectTById(Long id) {
        return clientExampleMapper.selectTById(id);
    }

    @Override
    public List<ClientExample> selectTList(ClientExample clientExample) {
        return clientExampleMapper.selectTList(clientExample);
    }

    @Override
    public int insertT(ClientExample clientExample) {
        return clientExampleMapper.insertT(clientExample);
    }

    @Override
    public int updateT(ClientExample clientExample) {
        return clientExampleMapper.updateT(clientExample);
    }

    @Override
    public int deleteTByIds(Long[] ids) {
        return clientExampleMapper.deleteTByIds(ids);
    }

    @Override
    public int deleteTById(Long id) {
        return clientExampleMapper.deleteTById(id);
    }
}
