package com.yanxi.yxregiserve.web.service.system.impl;

import com.yanxi.yxregiserve.web.domin.system.ConfigData;
import com.yanxi.yxregiserve.web.mapper.system.ConfigDataMapper;
import com.yanxi.yxregiserve.web.service.system.ConfigDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ConfigDataServiceImpl implements ConfigDataService {

    @Autowired
    public ConfigDataMapper configDataMapper;

    @Override
    public ConfigData selectTById(Long id) {
        return configDataMapper.selectTById(id);
    }

    @Override
    public List<ConfigData> selectTList(ConfigData configData) {
        return configDataMapper.selectTList(configData);
    }

    @Override
    public int insertT(ConfigData configData) {
        return configDataMapper.insertT(configData);
    }

    @Override
    public int updateT(ConfigData configData) {
        return configDataMapper.updateT(configData);
    }

    @Override
    public int deleteTByIds(Long[] ids) {
        return configDataMapper.deleteTByIds(ids);
    }

    @Override
    public int deleteTById(Long id) {
        return configDataMapper.deleteTById(id);
    }

}
