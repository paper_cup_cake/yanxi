package com.yanxi.yxregiserve.web.domin.system;

import com.yanxi.yxregiserve.web.domin.model.BaseEntity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ConfigData extends BaseEntity {

    /**
     * 自增id
     */
    private Integer id;

    /**
     * key
     */
    private String key;

    /**
     * value
     */
    private String value;

    /**
     * 标签
     */
    private String tag;

    /**
     * 备注
     */
    private String remark;

    /**
     * 顺序
     */
    private Integer num;


}
