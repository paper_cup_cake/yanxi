package com.yanxi.yxregiserve;

import com.yanxi.yxregiserve.web.domin.system.ConfigData;
import com.yanxi.yxregiserve.web.service.system.ConfigDataService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import java.util.List;

@SpringBootTest
class YxRegiserveApplicationTests {

    @Autowired
    private ConfigDataService configDataService;

    @Test
    void contextLoads() {
        List<ConfigData> list = configDataService.selectTList(ConfigData.builder().build());
        System.out.println(list);
    }

}
