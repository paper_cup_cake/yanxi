-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: 172.17.0.207    Database: yx-regiserve
-- ------------------------------------------------------
-- Server version	5.7.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client_example`
--

DROP TABLE IF EXISTS `client_example`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_example` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `project_name` varchar(100) DEFAULT NULL COMMENT '项目名',
  `port` varchar(100) DEFAULT NULL COMMENT '端口',
  `callback_interface` varchar(256) DEFAULT NULL COMMENT '健康检测回调接口',
  `in_net_ip` varchar(100) DEFAULT NULL COMMENT '内网ip',
  `out_net_ip` varchar(100) DEFAULT NULL COMMENT '外网ip',
  `reg_ser_time` datetime DEFAULT NULL COMMENT '注册时间',
  `ser_type` varchar(100) DEFAULT NULL COMMENT '实例状态 0-健康 1-异常 2-死亡',
  `except_time` text COMMENT '异常时间 字符串 异常时间拼接',
  `death_time` datetime DEFAULT NULL COMMENT '死亡时间',
  `check_num` int(11) DEFAULT '0' COMMENT '检测次数',
  `exce_num` int(11) DEFAULT '0' COMMENT '异常次数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='客户端实例表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_example`
--

LOCK TABLES `client_example` WRITE;
/*!40000 ALTER TABLE `client_example` DISABLE KEYS */;
INSERT INTO `client_example` VALUES (1,'user','8091','','localhost',NULL,'2023-11-14 11:53:36','2',NULL,'2023-11-14 12:05:39',NULL,NULL),(2,'user','8091','','localhost',NULL,'2023-11-14 12:05:39','2',NULL,'2023-11-14 14:48:32',NULL,NULL),(3,'user','8091','','localhost',NULL,'2023-11-14 14:48:32','2','null,Tue Nov 14 14:54:27 CST 2023,Tue Nov 14 14:55:09 CST 2023,Tue Nov 14 14:56:00 CST 2023,Tue Nov 14 14:57:00 CST 2023,Tue Nov 14 14:58:00 CST 2023,Tue Nov 14 14:59:00 CST 2023,Tue Nov 14 15:00:00 CST 2023,Tue Nov 14 15:01:00 CST 2023,Tue Nov 14 15:02:00 CST 2023,Tue Nov 14 15:03:00 CST 2023,Tue Nov 14 15:04:00 CST 2023',NULL,11,11),(4,'user','8091','','localhost',NULL,'2023-11-14 15:11:02','2','null,Tue Nov 14 15:12:30 CST 2023','2023-11-14 15:12:31',1,1),(5,'user','8091','','localhost',NULL,'2023-11-14 15:12:31','0','null,Tue Nov 14 15:13:00 CST 2023,Tue Nov 14 15:14:00 CST 2023,Tue Nov 14 15:15:00 CST 2023',NULL,3,3);
/*!40000 ALTER TABLE `client_example` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config_data`
--

DROP TABLE IF EXISTS `config_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `key` varchar(500) DEFAULT NULL COMMENT 'key',
  `value` varchar(2000) DEFAULT NULL COMMENT 'value',
  `tag` varchar(32) DEFAULT NULL COMMENT '标签',
  `remark` varchar(1080) DEFAULT NULL COMMENT '备注',
  `num` int(11) DEFAULT NULL COMMENT '顺序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COMMENT='配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config_data`
--

LOCK TABLES `config_data` WRITE;
/*!40000 ALTER TABLE `config_data` DISABLE KEYS */;
INSERT INTO `config_data` VALUES (1,'yx.name','user','user','项目名',1),(2,'yx.version','1.0','user','项目版本',2),(3,'server.name','user','user','服务名',3),(4,'server.port','8081','user','服务端口',4),(5,'grpc.server.port','8091','user','grpc开放端口',5),(7,'yx.name','regiserver','regiserver','项目名',1),(8,'yx.version','1.0','regiserver','项目版本',2),(9,'server.name','regiserver','regiserver','服务名',3),(10,'server.port','8080','regiserver','服务端口',4),(11,'grpc.server.port','8090','regiserver','grpc开放端口',5),(12,'spring.datasource.driver-class-name','com.mysql.cj.jdbc.Driver','regiserver','数据源配置',6),(13,'spring.datasource.url','jdbc:mysql://172.17.0.207:3306/yx-regiserve?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8','regiserver','数据源配置',7),(14,'spring.datasource.username','yx-regiserve','regiserver','数据源配置',8),(15,'spring.datasource.password','aX3W2ELGZfdKJEJc','regiserver','数据源配置',9),(16,'mybatis.typeAliasesPackage','com.yanxi.yxregiserve.web.domin.**','regiserver','MyBatis配置',10),(17,'mybatis.mapperLocation','classpath*:mapper/**/*Mapper.xml','regiserver','MyBatis配置',11),(18,'mybatis.configLocation','classpath:mybatis/mybatis-config.xml','regiserver','MyBatis配置',12),(20,'grpc.client.xyregiserve.port','8090','user','公共配置 rpc端口',1),(21,'grpc.client.xyregiserve.url','localhost','user','公共配置 rpc地址',2);
/*!40000 ALTER TABLE `config_data` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-11-15 15:17:29
