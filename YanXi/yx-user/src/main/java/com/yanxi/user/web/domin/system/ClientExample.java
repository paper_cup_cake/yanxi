package com.yanxi.user.web.domin.system;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.yanxi.user.web.domin.model.BaseEntity;
import lombok.Builder;
import lombok.Data;


import java.util.Date;

@Data
@Builder
public class ClientExample extends BaseEntity {

    /** 自增id */
    private Long id;

    /** 项目名 */
    private String projectName;

    /** 端口 */
    private String port;

    /** 健康检测回调接口 */
    private String callbackInterface;

    /** 内网ip */
    private String inNetIp;

    /** 外网ip */
    private String outNetIp;

    /** 注册时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date regSerTime;

    /** 实例状态 0-健康 1-异常 2-死亡 */
    private String serType;

    /** 异常时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date exceptTime;

    /** 死亡时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date deathTime;

    /** 检测次数 */
    private Long checkNum;

    /** 异常次数 */
    private Long exceNum;
}
