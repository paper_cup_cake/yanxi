package com.yanxi.user.web.study.dataStructure;

import java.util.Arrays;

/**
 * 简单选择排序
 */
public class SelectionSort {
    public static void selectionSort(int[] array) {
        int n = array.length;

        for (int i = 0; i < n - 1; i++) {
            // 假设当前索引i处的元素是最小的
            int minIndex = i;

            // 在未排序的部分中找到最小元素的索引
            for (int j = i + 1; j < n; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }

            // 将找到的最小元素与当前位置元素交换
            int temp = array[minIndex];
            array[minIndex] = array[i];
            array[i] = temp;
        }
    }

    public static void main(String[] args) {
        int[] myArray = {64, 25, 12, 22, 11};
        System.out.println("原始数组：" + Arrays.toString(myArray));

        selectionSort(myArray);

        System.out.println("排序后的数组：" + Arrays.toString(myArray));
    }
}
