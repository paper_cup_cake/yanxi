package com.yanxi.user.web.common.constant;


/**
 * 通用常量信息
 * 
 * @author shi
 */
public class GrpcConstants
{

    /**
     * 通用成功标识
     */
    public static final Integer RPC_SUCCESS_CODE = 200;

    /**
     * 通用失败标识
     */
    public static final Integer RPC_FAIL_CODE = 500;

    /**
     * 通用成功字符串
     */
    public static final String RPC_SUCCESS = "Success";

    /**
     * 通用失败字符串
     */
    public static final String RPC_FAIL = "Error";
}