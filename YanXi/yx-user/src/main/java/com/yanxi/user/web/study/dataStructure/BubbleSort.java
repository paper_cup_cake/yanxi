package com.yanxi.user.web.study.dataStructure;

import java.util.Arrays;

/**
 * 冒泡排序
 */
public class BubbleSort {
    public static void bubbleSort(int[] array) {
        int n = array.length;

        /**
         * 用于标记是否发生过交换
         */
        boolean swapped;

        for (int i = 0; i < n - 1; i++) {
            swapped = false;

            for (int j = 0; j < n - i - 1; j++) {

                /**
                 *  > 逆序
                 *
                 *  < 顺序
                 */

                if (array[j] > array[j + 1]) {
                    /**
                     * 交换array[j]和array[j+1]的位置
                     */
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    swapped = true;
                }
            }

            /**
             * 优化
             * 如果一轮遍历没有发生交换，说明数组已经有序，可以提前结束
             */
            if (!swapped) {
                break;
            }
        }
    }

    public static void main(String[] args) {
        int[] myArray = {64, 34, 25, 12, 22, 11, 90};
        System.out.println("原始数组：" + Arrays.toString(myArray));

        bubbleSort(myArray);

        System.out.println("排序后的数组：" + Arrays.toString(myArray));
    }
}
