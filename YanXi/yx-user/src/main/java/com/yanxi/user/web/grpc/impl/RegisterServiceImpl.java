package com.yanxi.user.web.grpc.impl;

import com.yanxi.user.web.common.constant.GrpcConstants;
import com.yanxi.user.web.grpc.RegisterService.HealthRequest;
import com.yanxi.user.web.grpc.RegisterService.HealthResponse;
import com.yanxi.user.web.grpc.RegisterService.RegisterServiceGrpc;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;


@GrpcService
public class RegisterServiceImpl extends RegisterServiceGrpc.RegisterServiceImplBase {


    /**
     * 健康检测接口
     * @param request
     * @param responseStreamObserver
     */
    @Override
    public void healthcheck(HealthRequest request, StreamObserver<HealthResponse> responseStreamObserver){

        /**
         * 成功返回
         */
        responseStreamObserver.onNext(HealthResponse.newBuilder()
                .setStatus(GrpcConstants.RPC_SUCCESS_CODE)
                .build());

        responseStreamObserver.onCompleted();
    }

}
