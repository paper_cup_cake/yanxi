package com.yanxi.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YxUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(YxUserApplication.class, args);
        System.out.println("(⑅•ᴗ•⑅)◜..°♡  YanXi项目 启动成功  ❛‿˂̵✧");
    }

}
