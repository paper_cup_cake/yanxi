package com.yanxi.user.web.grpc.pullConfigService;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.34.1)",
    comments = "Source: pull_config.proto")
public final class PullConfigServiceGrpc {

  private PullConfigServiceGrpc() {}

  public static final String SERVICE_NAME = "PullConfigService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.yanxi.user.web.grpc.pullConfigService.PullConfigRequest,
      com.yanxi.user.web.grpc.pullConfigService.PullConfigResponse> getGetConfigByTagMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getConfigByTag",
      requestType = com.yanxi.user.web.grpc.pullConfigService.PullConfigRequest.class,
      responseType = com.yanxi.user.web.grpc.pullConfigService.PullConfigResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.yanxi.user.web.grpc.pullConfigService.PullConfigRequest,
      com.yanxi.user.web.grpc.pullConfigService.PullConfigResponse> getGetConfigByTagMethod() {
    io.grpc.MethodDescriptor<com.yanxi.user.web.grpc.pullConfigService.PullConfigRequest, com.yanxi.user.web.grpc.pullConfigService.PullConfigResponse> getGetConfigByTagMethod;
    if ((getGetConfigByTagMethod = PullConfigServiceGrpc.getGetConfigByTagMethod) == null) {
      synchronized (PullConfigServiceGrpc.class) {
        if ((getGetConfigByTagMethod = PullConfigServiceGrpc.getGetConfigByTagMethod) == null) {
          PullConfigServiceGrpc.getGetConfigByTagMethod = getGetConfigByTagMethod =
              io.grpc.MethodDescriptor.<com.yanxi.user.web.grpc.pullConfigService.PullConfigRequest, com.yanxi.user.web.grpc.pullConfigService.PullConfigResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getConfigByTag"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yanxi.user.web.grpc.pullConfigService.PullConfigRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.yanxi.user.web.grpc.pullConfigService.PullConfigResponse.getDefaultInstance()))
              .setSchemaDescriptor(new PullConfigServiceMethodDescriptorSupplier("getConfigByTag"))
              .build();
        }
      }
    }
    return getGetConfigByTagMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static PullConfigServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<PullConfigServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<PullConfigServiceStub>() {
        @java.lang.Override
        public PullConfigServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new PullConfigServiceStub(channel, callOptions);
        }
      };
    return PullConfigServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static PullConfigServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<PullConfigServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<PullConfigServiceBlockingStub>() {
        @java.lang.Override
        public PullConfigServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new PullConfigServiceBlockingStub(channel, callOptions);
        }
      };
    return PullConfigServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static PullConfigServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<PullConfigServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<PullConfigServiceFutureStub>() {
        @java.lang.Override
        public PullConfigServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new PullConfigServiceFutureStub(channel, callOptions);
        }
      };
    return PullConfigServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class PullConfigServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getConfigByTag(com.yanxi.user.web.grpc.pullConfigService.PullConfigRequest request,
        io.grpc.stub.StreamObserver<com.yanxi.user.web.grpc.pullConfigService.PullConfigResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetConfigByTagMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetConfigByTagMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.yanxi.user.web.grpc.pullConfigService.PullConfigRequest,
                com.yanxi.user.web.grpc.pullConfigService.PullConfigResponse>(
                  this, METHODID_GET_CONFIG_BY_TAG)))
          .build();
    }
  }

  /**
   */
  public static final class PullConfigServiceStub extends io.grpc.stub.AbstractAsyncStub<PullConfigServiceStub> {
    private PullConfigServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PullConfigServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new PullConfigServiceStub(channel, callOptions);
    }

    /**
     */
    public void getConfigByTag(com.yanxi.user.web.grpc.pullConfigService.PullConfigRequest request,
        io.grpc.stub.StreamObserver<com.yanxi.user.web.grpc.pullConfigService.PullConfigResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetConfigByTagMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class PullConfigServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<PullConfigServiceBlockingStub> {
    private PullConfigServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PullConfigServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new PullConfigServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.yanxi.user.web.grpc.pullConfigService.PullConfigResponse getConfigByTag(com.yanxi.user.web.grpc.pullConfigService.PullConfigRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetConfigByTagMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class PullConfigServiceFutureStub extends io.grpc.stub.AbstractFutureStub<PullConfigServiceFutureStub> {
    private PullConfigServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected PullConfigServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new PullConfigServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.yanxi.user.web.grpc.pullConfigService.PullConfigResponse> getConfigByTag(
        com.yanxi.user.web.grpc.pullConfigService.PullConfigRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetConfigByTagMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_CONFIG_BY_TAG = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final PullConfigServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(PullConfigServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_CONFIG_BY_TAG:
          serviceImpl.getConfigByTag((com.yanxi.user.web.grpc.pullConfigService.PullConfigRequest) request,
              (io.grpc.stub.StreamObserver<com.yanxi.user.web.grpc.pullConfigService.PullConfigResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class PullConfigServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    PullConfigServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.yanxi.user.web.grpc.pullConfigService.PullConfig.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("PullConfigService");
    }
  }

  private static final class PullConfigServiceFileDescriptorSupplier
      extends PullConfigServiceBaseDescriptorSupplier {
    PullConfigServiceFileDescriptorSupplier() {}
  }

  private static final class PullConfigServiceMethodDescriptorSupplier
      extends PullConfigServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    PullConfigServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (PullConfigServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new PullConfigServiceFileDescriptorSupplier())
              .addMethod(getGetConfigByTagMethod())
              .build();
        }
      }
    }
    return result;
  }
}
